import "./App.css";
import React from "react";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import Frame6 from "./components/Frame6";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/:path(|frame-6)">
          <Frame6 {...frame6Data} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
const icroundRadioButtonUncheckedData = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked2Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked22Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-203@2x.svg",
};

const icroundRadioButtonUnchecked3Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked4Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked23Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-203@2x.svg",
};

const icroundRadioButtonUnchecked5Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked6Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked24Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-203@2x.svg",
};

const icroundRadioButtonUnchecked7Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked8Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked25Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-203@2x.svg",
};

const icroundRadioButtonUnchecked9Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked10Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked26Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-295@2x.svg",
};

const group6Data = {
    icroundRadioButtonUncheckedProps: icroundRadioButtonUnchecked9Data,
    icroundRadioButtonUnchecked2Props: icroundRadioButtonUnchecked26Data,
};

const icroundRadioButtonUnchecked11Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked12Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-136@2x.svg",
};

const icroundRadioButtonUnchecked27Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-295@2x.svg",
};

const group62Data = {
    icroundRadioButtonUncheckedProps: icroundRadioButtonUnchecked11Data,
    icroundRadioButtonUnchecked2Props: icroundRadioButtonUnchecked27Data,
};

const icroundRadioButtonUnchecked32Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-299@2x.svg",
};

const icroundRadioButtonUnchecked42Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-300@2x.svg",
};

const icroundRadioButtonUnchecked52Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-303@2x.svg",
};

const icroundRadioButtonUnchecked33Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-301@2x.svg",
};

const icroundRadioButtonUnchecked34Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-302@2x.svg",
};

const icroundRadioButtonUnchecked53Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-304@2x.svg",
};

const icroundRadioButtonUnchecked43Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-305@2x.svg",
};

const icroundRadioButtonUnchecked44Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-306@2x.svg",
};

const icroundRadioButtonUnchecked62Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-307@2x.svg",
};

const icroundRadioButtonUnchecked63Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-307@2x.svg",
};

const icroundRadioButtonUnchecked72Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-309@2x.svg",
};

const icroundRadioButtonUnchecked82Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-310@2x.svg",
};

const icroundRadioButtonUnchecked92Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-313@2x.svg",
};

const icroundRadioButtonUnchecked83Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-311@2x.svg",
};

const icroundRadioButtonUnchecked73Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-312@2x.svg",
};

const icroundRadioButtonUnchecked93Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-314@2x.svg",
};

const icroundRadioButtonUnchecked74Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-315@2x.svg",
};

const icroundRadioButtonUnchecked84Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-316@2x.svg",
};

const frame6Data = {
    rectangle28: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/rectangle-4@1x.svg",
    rectangle29: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/rectangle-4@1x.svg",
    anticovi: "AntiCoVi",
    thngTinDchT: "Thông tin dịch tễ",
    bnDchT: "Bản đồ dịch tễ",
    tinTc: "Tin tức",
    rectangle30: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/rectangle-1@2x.svg",
    rectangle31: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/rectangle-2@2x.svg",
    logIn: "Log in",
    signUp: "Sign up",
    giPhnHi: "Gửi phản hồi",
    rectangle24: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-24@1x.svg",
    spanText: "Họ và tên (ghi chữ IN HOA)",
    spanText2: "(*)",
    khaiH: "Khai hộ",
    text2: "Địa chỉ liên lạc tại Việt Nam",
    text3: "Có thẻ bảo hiểm y tế",
    spanText3: "Tỉnh thành ",
    spanText4: "(*)",
    spanText5: "Sốt ",
    spanText6: "(*)",
    spanText7: "Ho ",
    spanText8: "(*)",
    spanText9: "Khó thở ",
    spanText10: "(*)",
    spanText11: "Viêm phổi ",
    spanText12: "(*)",
    spanText13: "Đau họng ",
    spanText14: "(*)",
    spanText15: "Mệt mỏi ",
    spanText16: "(*)",
    spanText17: "Số nhà, phố, tổ dân phố/thôn/đội",
    spanText18: " (*)",
    triuChng: "Triệu chứng",
    c: "Có",
    khng: "Không",
    spanText19: "Trong vòng 14 ngày qua, anh chị có đến tỉnh/thành phố, quốc gia/vùng lãnh thổ nào (Có thể đi qua nhiều nơi) ",
    spanText20: "(*)",
    spanText21: "Trong vòng 14 ngày qua Anh/Chị có thấy xuất hiện dấu hiệu nào sau đây không ? ",
    spanText22: "(*)",
    email: "Email",
    spanText23: "Điện thoại ",
    spanText24: "(*)",
    spanText25: "Quận / huyện ",
    spanText26: "(*)",
    phngX: "Phường / xã (*)",
    spanText27: "Số hộ chiếu / CMND / CCCD ",
    spanText28: "(*)",
    spanText29: "Năm sinh ",
    spanText30: "(*)",
    spanText31: "Giới tính ",
    spanText32: "(*)",
    spanText33: "Quốc tịch",
    spanText34: " (*)",
    vector2: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-2-1@2x.svg",
    vector4: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-2-1@2x.svg",
    vector3: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-2-1@2x.svg",
    vector5: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/vector-2@2x.svg",
    vector6: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-198@2x.svg",
    giTKhai: "GỬI TỜ KHAI",
    capture1: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/capture-1@2x.png",
    vector7: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-211@2x.svg",
    layer1: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/layer-1@2x.svg",
    vector8: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-248@2x.svg",
    vector9: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-212@2x.svg",
    vector10: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-173@2x.svg",
    vector11: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-214@2x.svg",
    vector12: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-215@2x.svg",
    vector13: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-216@2x.svg",
    vector14: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-217@2x.svg",
    vector15: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-178@2x.svg",
    vector16: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-219@2x.svg",
    vector17: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-220@2x.svg",
    vector18: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-181@2x.svg",
    vector19: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-222@2x.svg",
    vector20: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-223@2x.svg",
    vector21: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-224@2x.svg",
    vector22: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-225@2x.svg",
    vector23: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-226@2x.svg",
    vector24: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-187@2x.svg",
    vector25: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-228@2x.svg",
    vector26: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-229@2x.svg",
    vector27: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-230@2x.svg",
    vector28: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-231@2x.svg",
    vector29: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-232@2x.svg",
    vector30: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-193@2x.svg",
    vector31: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-234@2x.svg",
    vector32: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-235@2x.svg",
    vector33: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-196@2x.svg",
    vector34: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-237@2x.svg",
    vector35: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-238@2x.svg",
    vector36: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-199@2x.svg",
    vector37: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-200@2x.svg",
    vector38: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-241@2x.svg",
    vector39: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-202@2x.svg",
    vector40: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-203@2x.svg",
    vector41: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-244@2x.svg",
    vector42: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-245@2x.svg",
    vector43: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-246@2x.svg",
    vector44: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-247@2x.svg",
    vector45: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-231@2x.svg",
    vector46: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-272@2x.svg",
    title: "OAP",
    vector47: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-209@2x.svg",
    vector48: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-210@2x.svg",
    vector49: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-251@2x.svg",
    vector50: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-212@2x.svg",
    vector51: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-253@2x.svg",
    vector52: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-214@2x.svg",
    vector53: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-255@2x.svg",
    vector54: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-256@2x.svg",
    vector55: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-217@2x.svg",
    vector56: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-218@2x.svg",
    vector57: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-219@2x.svg",
    vector58: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-220@2x.svg",
    vector59: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-221@2x.svg",
    vector60: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-233@2x.svg",
    vector61: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-262@2x.svg",
    vector62: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-263@2x.svg",
    vector63: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-264@2x.svg",
    vector64: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-265@2x.svg",
    vector65: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-266@2x.svg",
    vector66: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-227@2x.svg",
    vector67: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-268@2x.svg",
    vector68: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-229@2x.svg",
    vector69: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-230@2x.svg",
    overlapGroup4: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-292@2x.svg",
    vector70: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-274@2x.svg",
    vector71: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-275@2x.svg",
    vector72: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-259@2x.svg",
    vector73: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60813247dbe6891a858e2dae/img/vector-252@2x.svg",
    vector74: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-261@2x.svg",
    vector75: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-279@2x.svg",
    vector76: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-280@2x.svg",
    vector77: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-281@2x.svg",
    vector78: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-282@2x.svg",
    vector79: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-283@2x.svg",
    vector80: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-284@2x.svg",
    vector81: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-285@2x.svg",
    vector82: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-269@2x.svg",
    vector83: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-287@2x.svg",
    vector84: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-288@2x.svg",
    vector85: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-272@2x.svg",
    vector86: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-273@2x.svg",
    vector87: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812cfe200c27f99805dc45/img/vector-274@2x.svg",
    layer12: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/layer-1-1@2x.svg",
    spanText35: <>THÔNG TIN KHAI BÁO Y TẾ<br />(PHÒNG CHỐNG DỊCH COVID-19)<br /></>,
    spanText36: "Khuyến cáo: Khai báo thông tin sai là vi phạm pháp luật Việt Nam và có thể xử lý hình sự",
    spanText37: "Người bệnh hoặc nghi ngờ, mắc bệnh COVID-19 ",
    spanText38: "(*)",
    spanText39: "Người từ nước có bệnh COVID-19 ",
    spanText40: "(*)",
    spanText41: "Người có biểu hiện (Sốt, ho, khó thở , Viêm phổi)  ",
    spanText42: "(*)",
    spanText43: "Mã bảo mật ",
    spanText44: "(*)",
    c2: "Có",
    khng2: "Không",
    spanText45: "Trong vòng 14 ngày qua, Anh/Chị có tiếp xúc với ",
    spanText46: "(*)",
    spanText47: "Hiện tại Anh/Chị có các bệnh nào dưới đây ",
    spanText48: "(*)",
    text14: `Dữ liệu bạn cung cấp hoàn toàn bảo mật và chỉ phục vụ cho việc phòng chống dịch, thuộc quản lý của Ban chỉ đạo quốc gia về Phòng chống dịch Covid-19. Khi bạn nhấn nút "Gửi" là bạn đã hiểu và đồng ý.`,
    spanText49: "Bệnh gan mãn tĩnh ",
    spanText50: "(*)",
    spanText51: "Huyết áp cao ",
    spanText52: " (*)",
    spanText53: "Người nhận ghép tạng , Thủy xương ",
    spanText54: "(*)",
    spanText55: "Suy giảm miễn dịch ",
    spanText56: "(*)",
    spanText57: "Ung thư ",
    spanText58: "(*)",
    spanText59: "Bạn có đang trong thời gian thai kỳ hay không? ",
    spanText60: "(*)",
    spanText61: "Tiểu đường  ",
    spanText62: "(*)",
    spanText63: "Bệnh máu mãn tính ",
    spanText64: "(*)",
    spanText65: "Bệnh tim mạch   ",
    spanText66: "(*)",
    c3: "Có",
    khng3: "Không",
    icroundRadioButtonUncheckedProps: icroundRadioButtonUncheckedData,
    icroundRadioButtonUnchecked2Props: icroundRadioButtonUnchecked22Data,
    icroundRadioButtonUnchecked3Props: icroundRadioButtonUnchecked32Data,
    icroundRadioButtonUnchecked4Props: icroundRadioButtonUnchecked42Data,
    icroundRadioButtonUnchecked22Props: icroundRadioButtonUnchecked23Data,
    icroundRadioButtonUnchecked5Props: icroundRadioButtonUnchecked52Data,
    icroundRadioButtonUnchecked6Props: icroundRadioButtonUnchecked62Data,
    icroundRadioButtonUnchecked23Props: icroundRadioButtonUnchecked24Data,
    icroundRadioButtonUnchecked7Props: icroundRadioButtonUnchecked72Data,
    icroundRadioButtonUnchecked8Props: icroundRadioButtonUnchecked82Data,
    icroundRadioButtonUnchecked24Props: icroundRadioButtonUnchecked25Data,
    group6Props: group6Data,
    group62Props: group62Data,
    icroundRadioButtonUnchecked32Props: icroundRadioButtonUnchecked33Data,
    icroundRadioButtonUnchecked33Props: icroundRadioButtonUnchecked34Data,
    icroundRadioButtonUnchecked52Props: icroundRadioButtonUnchecked53Data,
    icroundRadioButtonUnchecked42Props: icroundRadioButtonUnchecked43Data,
    icroundRadioButtonUnchecked43Props: icroundRadioButtonUnchecked44Data,
    icroundRadioButtonUnchecked62Props: icroundRadioButtonUnchecked63Data,
    icroundRadioButtonUnchecked9Props: icroundRadioButtonUnchecked92Data,
    icroundRadioButtonUnchecked82Props: icroundRadioButtonUnchecked83Data,
    icroundRadioButtonUnchecked72Props: icroundRadioButtonUnchecked73Data,
    icroundRadioButtonUnchecked92Props: icroundRadioButtonUnchecked93Data,
    icroundRadioButtonUnchecked73Props: icroundRadioButtonUnchecked74Data,
    icroundRadioButtonUnchecked83Props: icroundRadioButtonUnchecked84Data,
};

