import React from "react";
import IcroundRadioButtonUnchecked from "../IcroundRadioButtonUnchecked";
import IcroundRadioButtonUnchecked2 from "../IcroundRadioButtonUnchecked2";
import Group6 from "../Group6";
import IcroundRadioButtonUnchecked3 from "../IcroundRadioButtonUnchecked3";
import IcroundRadioButtonUnchecked4 from "../IcroundRadioButtonUnchecked4";
import IcroundRadioButtonUnchecked5 from "../IcroundRadioButtonUnchecked5";
import IcroundRadioButtonUnchecked6 from "../IcroundRadioButtonUnchecked6";
import IcroundRadioButtonUnchecked7 from "../IcroundRadioButtonUnchecked7";
import IcroundRadioButtonUnchecked8 from "../IcroundRadioButtonUnchecked8";
import IcroundRadioButtonUnchecked9 from "../IcroundRadioButtonUnchecked9";
import "./Frame6.css";

function Frame6(props) {
  const {
    rectangle28,
    rectangle29,
    anticovi,
    thngTinDchT,
    bnDchT,
    tinTc,
    rectangle30,
    rectangle31,
    logIn,
    signUp,
    giPhnHi,
    rectangle24,
    spanText,
    spanText2,
    khaiH,
    text2,
    text3,
    spanText3,
    spanText4,
    spanText5,
    spanText6,
    spanText7,
    spanText8,
    spanText9,
    spanText10,
    spanText11,
    spanText12,
    spanText13,
    spanText14,
    spanText15,
    spanText16,
    spanText17,
    spanText18,
    triuChng,
    c,
    khng,
    spanText19,
    spanText20,
    spanText21,
    spanText22,
    email,
    spanText23,
    spanText24,
    spanText25,
    spanText26,
    phngX,
    spanText27,
    spanText28,
    spanText29,
    spanText30,
    spanText31,
    spanText32,
    spanText33,
    spanText34,
    vector2,
    vector4,
    vector3,
    vector5,
    vector6,
    giTKhai,
    capture1,
    vector7,
    layer1,
    vector8,
    vector9,
    vector10,
    vector11,
    vector12,
    vector13,
    vector14,
    vector15,
    vector16,
    vector17,
    vector18,
    vector19,
    vector20,
    vector21,
    vector22,
    vector23,
    vector24,
    vector25,
    vector26,
    vector27,
    vector28,
    vector29,
    vector30,
    vector31,
    vector32,
    vector33,
    vector34,
    vector35,
    vector36,
    vector37,
    vector38,
    vector39,
    vector40,
    vector41,
    vector42,
    vector43,
    vector44,
    vector45,
    vector46,
    title,
    vector47,
    vector48,
    vector49,
    vector50,
    vector51,
    vector52,
    vector53,
    vector54,
    vector55,
    vector56,
    vector57,
    vector58,
    vector59,
    vector60,
    vector61,
    vector62,
    vector63,
    vector64,
    vector65,
    vector66,
    vector67,
    vector68,
    vector69,
    overlapGroup4,
    vector70,
    vector71,
    vector72,
    vector73,
    vector74,
    vector75,
    vector76,
    vector77,
    vector78,
    vector79,
    vector80,
    vector81,
    vector82,
    vector83,
    vector84,
    vector85,
    vector86,
    vector87,
    layer12,
    spanText35,
    spanText36,
    spanText37,
    spanText38,
    spanText39,
    spanText40,
    spanText41,
    spanText42,
    spanText43,
    spanText44,
    c2,
    khng2,
    spanText45,
    spanText46,
    spanText47,
    spanText48,
    text14,
    spanText49,
    spanText50,
    spanText51,
    spanText52,
    spanText53,
    spanText54,
    spanText55,
    spanText56,
    spanText57,
    spanText58,
    spanText59,
    spanText60,
    spanText61,
    spanText62,
    spanText63,
    spanText64,
    spanText65,
    spanText66,
    c3,
    khng3,
    icroundRadioButtonUncheckedProps,
    icroundRadioButtonUnchecked2Props,
  
    icroundRadioButtonUnchecked3Props,
    icroundRadioButtonUnchecked4Props,
    icroundRadioButtonUnchecked22Props,
    icroundRadioButtonUnchecked5Props,
    icroundRadioButtonUnchecked6Props,
    icroundRadioButtonUnchecked23Props,
    icroundRadioButtonUnchecked7Props,
    icroundRadioButtonUnchecked8Props,
    icroundRadioButtonUnchecked24Props,
    group6Props,
    group62Props,


 
    icroundRadioButtonUnchecked32Props,
    icroundRadioButtonUnchecked33Props,
    icroundRadioButtonUnchecked52Props,
    icroundRadioButtonUnchecked42Props,
    icroundRadioButtonUnchecked43Props,

    icroundRadioButtonUnchecked62Props,
 
  
    icroundRadioButtonUnchecked9Props,
    icroundRadioButtonUnchecked82Props,
    icroundRadioButtonUnchecked72Props,
    icroundRadioButtonUnchecked92Props,
    icroundRadioButtonUnchecked73Props,
    icroundRadioButtonUnchecked83Props,
  } = props;

  return (
    <div class="container-center-horizontal">
      <div className="frame-6 screen">
        <div className="overlap-group">
          <div className="frame-7"></div>
          <img className="rectangle-28" src={rectangle28} />
          <img className="rectangle-29" src={rectangle29} />
          <div className="anti-co-vi roboto-bold-blue-whale-24px">{anticovi}</div>
          <div className="thng-tin-dch-t roboto-bold-blue-whale-18px">{thngTinDchT}</div>
          <div className="bn-dch-t roboto-bold-blue-whale-18px">{bnDchT}</div>
          <div className="tin-tc roboto-bold-blue-whale-18px">{tinTc}</div>
          <img
            className="vector-85"
            src="https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-196@2x.svg"
          />
          <img className="rectangle-30" src={rectangle30} />
          <img className="rectangle-31" src={rectangle31} />
          <div className="log-in roboto-bold-onahau-18px">{logIn}</div>
          <div className="sign-up roboto-bold-onahau-18px">{signUp}</div>
          <div className="gi-phn-hi roboto-bold-blue-whale-18px">{giPhnHi}</div>
          <div className="gi-phn-hi-1"></div>
          <img className="rectangle-24" src={rectangle24} />
          <div className="text-1 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText}</span>
              <span className="span1-1">{spanText2}</span>
            </span>
          </div>
          <div className="khai-h valign-text-middle robotoslab-regular-normal-black-18px">{khaiH}</div>
          <div className="text-2 valign-text-middle robotoslab-bold-black-18px">{text2}</div>
          <div className="text-3 valign-text-middle robotoslab-regular-normal-black-18px">{text3}</div>
          <div className="tnh-thnh valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText3}</span>
              <span className="span1-1">{spanText4}</span>
            </span>
          </div>
          <div className="date valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText5}</span>
              <span className="span1-1">{spanText6}</span>
            </span>
          </div>
          <div className="ho valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText7}</span>
              <span className="span1-1">{spanText8}</span>
            </span>
          </div>
          <div className="kh-th valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText9}</span>
              <span className="span1-1">{spanText10}</span>
            </span>
          </div>
          <div className="vim-phi valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText11}</span>
              <span className="span1-1">{spanText12}</span>
            </span>
          </div>
          <div className="au-hng valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText13}</span>
              <span className="span1-1">{spanText14}</span>
            </span>
          </div>
          <div className="mt-mi valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText15}</span>
              <span className="span1-1">{spanText16}</span>
            </span>
          </div>
          <div className="text-4 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText17}</span>
              <span className="span1-1">{spanText18}</span>
            </span>
          </div>
          <div className="triu-chng valign-text-middle robotoslab-bold-black-18px">{triuChng}</div>
          <div className="c-1 valign-text-middle robotoslab-bold-black-18px">{c}</div>
          <div className="khng-2 valign-text-middle robotoslab-bold-black-18px">{khng}</div>
          <div className="text-5 valign-text-middle robotoslab-bold-white-18px">
            <span>
              <span className="span0-2">{spanText19}</span>
              <span className="span1-2">{spanText20}</span>
            </span>
          </div>
          <div className="text-6 valign-text-middle robotoslab-bold-white-18px">
            <span>
              <span className="span0-2">{spanText21}</span>
              <span className="span1-2">{spanText22}</span>
            </span>
          </div>
          <div className="email valign-text-middle robotoslab-regular-normal-black-18px">{email}</div>
          <div className="in-thoi valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText23}</span>
              <span className="span1-1">{spanText24}</span>
            </span>
          </div>
          <div className="qun-huyn valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText25}</span>
              <span className="span1-1">{spanText26}</span>
            </span>
          </div>
          <div className="phng-x valign-text-middle robotoslab-regular-normal-black-18px">{phngX}</div>
          <div className="text-7 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText27}</span>
              <span className="span1-1">{spanText28}</span>
            </span>
          </div>
          <div className="nm-sinh valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText29}</span>
              <span className="span1-1">{spanText30}</span>
            </span>
          </div>
          <div className="gii-tnh valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText31}</span>
              <span className="span1-1">{spanText32}</span>
            </span>
          </div>
          <div className="quc-tch valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText33}</span>
              <span className="span1-1">{spanText34}</span>
            </span>
          </div>
          <div className="rectangle-36 border-1px-aqua-pearl"></div>
          <div className="rectangle-37 border-1px-aqua-pearl"></div>
          <div className="rectangle-33 border-1px-aqua-pearl"></div>
          <div className="rectangle-35 border-1px-aqua-pearl"></div>
          <div className="rectangle-38 border-1px-aqua-pearl"></div>
          <div className="rectangle-43 border-1px-aqua-pearl"></div>
          <div className="rectangle-44 border-1px-aqua-pearl"></div>
          <div className="rectangle-41 border-1px-aqua-pearl"></div>
          <div className="rectangle-39 border-1px-aqua-pearl"></div>
          <div className="rectangle-40 border-1px-aqua-pearl"></div>
          <div className="rectangle-42 border-1px-aqua-pearl"></div>
          <img className="vector-2" src={vector2} />
          <img className="vector-4" src={vector4} />
          <img className="vector-3" src={vector3} />
          <div className="rectangle-34 border-1px-aqua-pearl"></div>
          <img className="vector-74" src={vector5} />
          <img className="vector-22" src={vector6} />
          <div className="group-4">
            <IcroundRadioButtonUnchecked src={icroundRadioButtonUncheckedProps.src} />
            <IcroundRadioButtonUnchecked
              src={icroundRadioButtonUnchecked2Props.src}
              className="icround-radio-button-unchecked-7"
            />
            <IcroundRadioButtonUnchecked2 src={icroundRadioButtonUnchecked2Props.src} />
            <IcroundRadioButtonUnchecked
              src={icroundRadioButtonUnchecked3Props.src}
              className="icround-radio-button-unchecked-7"
            />
            <IcroundRadioButtonUnchecked
              src={icroundRadioButtonUnchecked4Props.src}
              className="icround-radio-button-unchecked-7"
            />
            <IcroundRadioButtonUnchecked2
              src={icroundRadioButtonUnchecked22Props.src}
              className="icround-radio-button-unchecked-12"
            />
          </div>
          <div className="group-2">
            <div className="overlap-group1">
              <IcroundRadioButtonUnchecked
                src={icroundRadioButtonUnchecked5Props.src}
                className="icround-radio-button-unchecked-10"
              />
              <div className="group-1-2">
                <IcroundRadioButtonUnchecked
                  src={icroundRadioButtonUnchecked6Props.src}
                  className="icround-radio-button-unchecked-8"
                />
                <div className="icround-radio-button-unchecked-1"></div>
                <IcroundRadioButtonUnchecked2
                  src={icroundRadioButtonUnchecked23Props.src}
                  className="icround-radio-button-unchecked-13"
                />
                <IcroundRadioButtonUnchecked
                  src={icroundRadioButtonUnchecked7Props.src}
                  className="icround-radio-button-unchecked-7"
                />
                <IcroundRadioButtonUnchecked
                  src={icroundRadioButtonUnchecked8Props.src}
                  className="icround-radio-button-unchecked-7"
                />
                <IcroundRadioButtonUnchecked2
                  src={icroundRadioButtonUnchecked24Props.src}
                  className="icround-radio-button-unchecked-12"
                />
              </div>
            </div>
          </div>
          <div className="rectangle-5"></div>
          <div className="gi-t-khai valign-text-middle roboto-normal-white-18px">{giTKhai}</div>
          <img className="capture-1" src={capture1} />
          <div className="rectangle-6 border-1px-aqua-pearl"></div>
          <div className="simple-line-iconsreload">
            <img className="vector-35" src={vector7} />
          </div>
          <div className="asset-9-1">
            <img className="layer-1-1" src={layer1} />
          </div>
          <div className="asset-13-1">
            <div className="overlap-group2-1">
              <img className="vector-46" src={vector8} />
              <img className="vector-12" src={vector9} />
              <img className="vector-9" src={vector10} />
              <img className="vector-53" src={vector11} />
              <img className="vector-69" src={vector12} />
              <img className="vector-15" src={vector13} />
              <img className="vector-47" src={vector14} />
              <img className="vector-42" src={vector15} />
              <img className="vector-17" src={vector16} />
              <img className="vector-19" src={vector17} />
              <img className="vector-40" src={vector18} />
              <img className="vector-68" src={vector19} />
              <img className="vector" src={vector20} />
              <img className="vector-75" src={vector21} />
              <img className="vector-41" src={vector22} />
              <img className="vector-30" src={vector23} />
              <img className="vector-18" src={vector24} />
              <img className="vector-83" src={vector25} />
              <img className="vector-28" src={vector26} />
              <img className="vector-70" src={vector27} />
              <img className="vector-37" src={vector28} />
              <img className="vector-71" src={vector29} />
              <img className="vector-29" src={vector30} />
              <img className="vector-50" src={vector31} />
              <img className="vector-57" src={vector32} />
              <img className="vector-76" src={vector33} />
              <img className="vector-66" src={vector34} />
              <img className="vector-1" src={vector35} />
              <img className="vector-48" src={vector36} />
              <img className="vector-81" src={vector37} />
              <img className="vector-38" src={vector38} />
              <img className="vector-13" src={vector39} />
              <img className="vector-78" src={vector40} />
              <img className="vector-25" src={vector41} />
              <img className="vector-54" src={vector42} />
              <img className="vector-67" src={vector43} />
              <img className="vector-59" src={vector44} />
            </div>
          </div>
          <div className="asset-12-1">
            <div className="overlap-group3">
              <img className="vector-77" src={vector45} />
              <img className="vector-24" src={vector46} />
              <h1 className="title">{title}</h1>
              <img className="vector-36" src={vector47} />
              <img className="vector-51" src={vector48} />
              <img className="vector-43" src={vector49} />
              <img className="vector-86" src={vector50} />
              <img className="vector-23" src={vector51} />
              <img className="vector-33" src={vector52} />
              <img className="vector-5" src={vector53} />
              <img className="vector-6" src={vector54} />
              <img className="vector-82" src={vector55} />
              <img className="vector-65" src={vector56} />
              <img className="vector-80" src={vector57} />
              <img className="vector-49" src={vector58} />
              <img className="vector-45" src={vector59} />
              <img className="vector-21" src={vector60} />
              <img className="vector-44" src={vector61} />
              <img className="vector-84" src={vector62} />
              <img className="vector-79" src={vector63} />
              <img className="vector-26" src={vector64} />
              <img className="vector-52" src={vector65} />
              <img className="vector-39" src={vector66} />
              <img className="vector-63" src={vector67} />
              <img className="vector-14" src={vector68} />
              <img className="vector-32" src={vector69} />
            </div>
          </div>
          <div className="asset-10-1">
            <div className="overlap-group4" style={{ backgroundImage: `url(${overlapGroup4})` }}>
              <img className="vector-56" src={vector70} />
              <img className="vector-61" src={vector71} />
              <img className="vector-34" src={vector72} />
              <img className="vector-27" src={vector73} />
              <img className="vector-31" src={vector74} />
              <img className="vector-16" src={vector75} />
              <img className="vector-11" src={vector76} />
              <img className="vector-58" src={vector77} />
              <img className="vector-55" src={vector78} />
              <img className="vector-73" src={vector79} />
              <img className="vector-8" src={vector80} />
              <img className="vector-7" src={vector81} />
              <img className="vector-64" src={vector82} />
              <img className="vector-60" src={vector83} />
              <img className="vector-62" src={vector84} />
              <img className="vector-72" src={vector85} />
              <img className="vector-20" src={vector86} />
              <img className="vector-10" src={vector87} />
            </div>
          </div>
          <div className="asset-8-1">
            <img className="layer-1" src={layer12} />
          </div>
          <div className="text-8 valign-text-middle">
            <span>
              <span className="span0">{spanText35}</span>
              <span className="span1">{spanText36}</span>
            </span>
          </div>
          <div className="text-9 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText37}</span>
              <span className="span1-1">{spanText38}</span>
            </span>
          </div>
          <div className="text-10 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText39}</span>
              <span className="span1-1">{spanText40}</span>
            </span>
          </div>
          <div className="text-11 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText41}</span>
              <span className="span1-1">{spanText42}</span>
            </span>
          </div>
          <div className="m-bo-mt valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText43}</span>
              <span className="span1-1">{spanText44}</span>
            </span>
          </div>
          <div className="c-2 valign-text-middle robotoslab-bold-black-18px">{c2}</div>
          <div className="khng valign-text-middle robotoslab-bold-black-18px">{khng2}</div>
          <div className="text-12 valign-text-middle robotoslab-bold-white-18px">
            <span>
              <span className="span0-2">{spanText45}</span>
              <span className="span1-2">{spanText46}</span>
            </span>
          </div>
          <div className="text-13 valign-text-middle robotoslab-bold-white-18px">
            <span>
              <span className="span0-2">{spanText47}</span>
              <span className="span1-2">{spanText48}</span>
            </span>
          </div>
          <div className="text-14 valign-text-middle">{text14}</div>
          <div className="icround-radio-button-unchecked-4"></div>
          <Group6
            icroundRadioButtonUncheckedProps={group6Props.icroundRadioButtonUncheckedProps}
            icroundRadioButtonUnchecked2Props={group6Props.icroundRadioButtonUnchecked2Props}
          />
          <Group6
            icroundRadioButtonUncheckedProps={group62Props.icroundRadioButtonUncheckedProps}
            icroundRadioButtonUnchecked2Props={group62Props.icroundRadioButtonUnchecked2Props}
            className="group-7"
          />
          <div className="icround-radio-button-unchecked-3"></div>
          <div className="text-15 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText49}</span>
              <span className="span1-1">{spanText50}</span>
            </span>
          </div>
          <div className="huyt-p-cao valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText51}</span>
              <span className="span1-1">{spanText52}</span>
            </span>
          </div>
          <div className="text-16 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText53}</span>
              <span className="span1-1">{spanText54}</span>
            </span>
          </div>
          <div className="text-17 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText55}</span>
              <span className="span1-1">{spanText56}</span>
            </span>
          </div>
          <div className="ung-th valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText57}</span>
              <span className="span1-1">{spanText58}</span>
            </span>
          </div>
          <div className="text-18 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText59}</span>
              <span className="span1-1">{spanText60}</span>
            </span>
          </div>
          <div className="tiu-ng valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText61}</span>
              <span className="span1-1">{spanText62}</span>
            </span>
          </div>
          <div className="text-19 valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText63}</span>
              <span className="span1-1">{spanText64}</span>
            </span>
          </div>
          <div className="bnh-tim-mch valign-text-middle robotoslab-regular-normal-white-18px">
            <span>
              <span className="span0-1">{spanText65}</span>
              <span className="span1-1">{spanText66}</span>
            </span>
          </div>
          <div className="c valign-text-middle robotoslab-bold-black-18px">{c3}</div>
          <div className="khng-1 valign-text-middle robotoslab-bold-black-18px">{khng3}</div>
          <div className="group-5">
            <div className="group-3">
              <div className="overlap-group1-1">
                <IcroundRadioButtonUnchecked3 src={icroundRadioButtonUnchecked3Props.src} />
                <div className="group-1">
                  <IcroundRadioButtonUnchecked4 src={icroundRadioButtonUnchecked4Props.src} />
                  <div className="overlap-group2">
                    <IcroundRadioButtonUnchecked5 src={icroundRadioButtonUnchecked5Props.src} />
                    <div className="icround-radio-button-unchecked"></div>
                  </div>
                  <IcroundRadioButtonUnchecked3
                    src={icroundRadioButtonUnchecked32Props.src}
                    className="icround-radio-button-unchecked-16"
                  />
                  <IcroundRadioButtonUnchecked3
                    src={icroundRadioButtonUnchecked33Props.src}
                    className="icround-radio-button-unchecked-16"
                  />
                  <IcroundRadioButtonUnchecked5
                    src={icroundRadioButtonUnchecked52Props.src}
                    className="icround-radio-button-unchecked-21"
                  />
                </div>
              </div>
            </div>
            <IcroundRadioButtonUnchecked4
              src={icroundRadioButtonUnchecked42Props.src}
              className="icround-radio-button-unchecked-20"
            />
            <IcroundRadioButtonUnchecked4
              src={icroundRadioButtonUnchecked43Props.src}
              className="icround-radio-button-unchecked-18"
            />
          </div>
          <IcroundRadioButtonUnchecked6 src={icroundRadioButtonUnchecked6Props.src} />
          <IcroundRadioButtonUnchecked6
            src={icroundRadioButtonUnchecked62Props.src}
            className="icround-radio-button-unchecked-24"
          />
          <div className="group-8">
            <div className="group-3">
              <div className="overlap-group1-2">
                <IcroundRadioButtonUnchecked7 src={icroundRadioButtonUnchecked7Props.src} />
                <div className="group-1-1">
                  <IcroundRadioButtonUnchecked8 src={icroundRadioButtonUnchecked8Props.src} />
                  <div className="icround-radio-button-unchecked-6"></div>
                  <IcroundRadioButtonUnchecked9 src={icroundRadioButtonUnchecked9Props.src} />
                  <IcroundRadioButtonUnchecked8
                    src={icroundRadioButtonUnchecked82Props.src}
                    className="icround-radio-button-unchecked-29"
                  />
                  <IcroundRadioButtonUnchecked7
                    src={icroundRadioButtonUnchecked72Props.src}
                    className="icround-radio-button-unchecked-27"
                  />
                  <IcroundRadioButtonUnchecked9
                    src={icroundRadioButtonUnchecked92Props.src}
                    className="icround-radio-button-unchecked-31"
                  />
                </div>
              </div>
            </div>
            <IcroundRadioButtonUnchecked7
              src={icroundRadioButtonUnchecked73Props.src}
              className="icround-radio-button-unchecked-26"
            />
            <IcroundRadioButtonUnchecked8
              src={icroundRadioButtonUnchecked83Props.src}
              className="icround-radio-button-unchecked-30"
            />
          </div>
          <div className="icround-radio-button-unchecked-2"></div>
          <div className="icround-radio-button-unchecked-5"></div>
        </div>
      </div>
    </div>
  );
}

export default Frame6;
