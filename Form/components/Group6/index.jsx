import React from "react";
import IcroundRadioButtonUnchecked from "../IcroundRadioButtonUnchecked";
import IcroundRadioButtonUnchecked2 from "../IcroundRadioButtonUnchecked2";
import "./Group6.css";

function Group6(props) {
  const {
    icroundRadioButtonUncheckedProps,
    icroundRadioButtonUnchecked2Props,

    className,
  } = props;

  return (
    <div className={`group-6 ${className || ""}`}>
      <IcroundRadioButtonUnchecked
        src={icroundRadioButtonUncheckedProps.src}
        className="icround-radio-button-unchecked-8"
      />
      <IcroundRadioButtonUnchecked
        src={icroundRadioButtonUnchecked2Props.src}
        className="icround-radio-button-unchecked-9"
      />
      <IcroundRadioButtonUnchecked2
        src={icroundRadioButtonUnchecked2Props.src}
        className="icround-radio-button-unchecked-14"
      />
    </div>
  );
}

export default Group6;
