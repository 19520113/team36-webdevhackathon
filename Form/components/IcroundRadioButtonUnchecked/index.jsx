import React from "react";
import "./IcroundRadioButtonUnchecked.css";

function IcroundRadioButtonUnchecked(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-11 ${className || ""}`}>
      <img className="vector-87" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked;
