import React from "react";
import "./IcroundRadioButtonUnchecked2.css";

function IcroundRadioButtonUnchecked2(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-15 ${className || ""}`}>
      <img className="vector-88" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked2;
