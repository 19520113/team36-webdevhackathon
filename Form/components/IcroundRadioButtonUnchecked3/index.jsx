import React from "react";
import "./IcroundRadioButtonUnchecked3.css";

function IcroundRadioButtonUnchecked3(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-17 ${className || ""}`}>
      <img className="vector-89" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked3;
