import React from "react";
import "./IcroundRadioButtonUnchecked4.css";

function IcroundRadioButtonUnchecked4(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-19 ${className || ""}`}>
      <img className="vector-90" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked4;
