import React from "react";
import "./IcroundRadioButtonUnchecked5.css";

function IcroundRadioButtonUnchecked5(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-22 ${className || ""}`}>
      <img className="vector-91" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked5;
