import React from "react";
import "./IcroundRadioButtonUnchecked6.css";

function IcroundRadioButtonUnchecked6(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-23 ${className || ""}`}>
      <img className="vector-92" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked6;
