import React from "react";
import "./IcroundRadioButtonUnchecked7.css";

function IcroundRadioButtonUnchecked7(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-25 ${className || ""}`}>
      <img className="vector-93" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked7;
