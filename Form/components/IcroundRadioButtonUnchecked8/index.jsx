import React from "react";
import "./IcroundRadioButtonUnchecked8.css";

function IcroundRadioButtonUnchecked8(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-28 ${className || ""}`}>
      <img className="vector-94" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked8;
