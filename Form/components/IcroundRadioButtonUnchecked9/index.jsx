import React from "react";
import "./IcroundRadioButtonUnchecked9.css";

function IcroundRadioButtonUnchecked9(props) {
  const { src, className } = props;

  return (
    <div className={`icround-radio-button-unchecked-32 ${className || ""}`}>
      <img className="vector-95" src={src} />
    </div>
  );
}

export default IcroundRadioButtonUnchecked9;
