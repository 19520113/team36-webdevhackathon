import "./App.css";
import React from "react";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import Frame1 from "./components/Frame1";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/:path(|frame-1)">
          <Frame1 {...frame1Data} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
const frame5Data = {
    sCaNhim: "Số ca nhiễm",
    angIuTr: "Đang điều trị",
    khi: "Khỏi",
    tVong: "Tử vong",
};

const frame52Data = {
    sCaNhim: "Số ca nhiễm",
    angIuTr: "Đang điều trị",
    khi: "Khỏi",
    tVong: "Tử vong",
};

const frame1Data = {
    rectangle4: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-4@1x.svg",
    poster: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/poster@1x.png",
    rectangle6: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-6@2x.svg",
    vitNam: "VIỆT NAM",
    address: "2 801",
    number: "272",
    address2: "2 490",
    number2: "35",
    thGii: "THẾ GIỚI",
    phone: "109 469 508",
    phone2: "25 341 731",
    phone3: "81 563 167",
    phone4: "2 413 158",
    text1: "Nghệ An: Chưa nhận được phản ứng nặng sau tiêm vắc-xin phòng COVID-19",
    phatHienNguocDoiVeTacDungPhuCuaVacX: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/phat-hien-nguoc-doi-ve-tac-dung-phu-cua-vac-xin-covid-19-1@1x.png",
    anticovi: "AntiCoVi",
    thngTinDchT: "Thông tin dịch tễ",
    giPhnHi: "Gửi phản hồi",
    bnDchT: "Bản đồ dịch tễ",
    tinTc: "Tin tức",
    rectangle11: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-11@2x.svg",
    rectangle12: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-12@2x.svg",
    logIn: "Log in",
    signUp: "Sign up",
    tinTc2: "TIN TỨC",
    vector2: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-2@2x.svg",
    khaiBoYT: "KHAI BÁO Y TẾ",
    frame5Props: frame5Data,
    frame52Props: frame52Data,
};

