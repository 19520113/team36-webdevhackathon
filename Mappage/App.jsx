import "./App.css";
import React from "react";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import Frame5 from "./components/Frame5";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/:path(|frame-5)">
          <Frame5 {...frame5Data} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
const elmapMarkerData = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-134@2x.svg",
};

const elmapMarker2Data = {
    src: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-134@2x.svg",
};

const frame5Data = {
    rectangle4: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-4-2@1x.svg",
    rectangle18: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-4-1@1x.svg",
    title: "AntiCoVi",
    thngTinDchT: "Thông tin dịch tễ",
    giPhnHi: "Gửi phản hồi",
    bnDchT: "Bản đồ dịch tễ",
    tinTc: "Tin tức",
    rectangle1: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-1@2x.svg",
    rectangle2: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-2@2x.svg",
    logIn: "Log in",
    signUp: "Sign up",
    text1: "Vùng dịch gần nhất hiện tại hiện tại",
    polygon4: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/polygon-4@2x.svg",
    tnhTp: "Tỉnh/TP",
    text2: "Nhập khu vực bạn muốn biết tình hình dịch",
    vector2: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-1@1x.svg",
    polygon8: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/polygon-4@2x.svg",
    phngX: "Phường/Xã",
    polygon7: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/polygon-4@2x.svg",
    qunHuyn: "Quận/Huyện",
    vector3: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-136@2x.svg",
    vector4: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-137@2x.svg",
    vector5: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-138@2x.svg",
    vector6: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-139@2x.svg",
    vector7: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-140@2x.svg",
    vector8: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-141@2x.svg",
    vector9: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-142@2x.svg",
    vector10: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector@2x.png",
    vector11: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector@2x.png",
    vector12: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-145@2x.svg",
    vector13: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-146@2x.svg",
    vector14: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-195@2x.svg",
    vector15: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-147@2x.svg",
    vector16: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-148@2x.svg",
    vector17: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-149@2x.svg",
    vector18: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-150@2x.svg",
    vector19: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-151@2x.svg",
    vector20: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-152@2x.svg",
    vector21: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-153@2x.svg",
    vector22: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-154@2x.svg",
    vector23: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-155@2x.svg",
    vector24: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-156@2x.svg",
    vector25: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-157@2x.svg",
    vector26: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-158@2x.svg",
    vector27: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-159@2x.svg",
    vector28: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-160@2x.svg",
    vector29: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-161@2x.svg",
    vector30: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-162@2x.svg",
    vector31: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-163@2x.svg",
    vector32: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-164@2x.svg",
    vector33: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-165@2x.svg",
    vector34: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-166@2x.svg",
    vector35: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-167@2x.svg",
    vector36: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-168@2x.svg",
    vector37: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-169@2x.svg",
    vector38: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-170@2x.svg",
    vector39: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-171@2x.svg",
    vector40: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-172@2x.svg",
    vector41: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-173@2x.svg",
    vector42: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-174@2x.svg",
    vector43: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-175@2x.svg",
    vector44: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-176@2x.svg",
    vector45: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-177@2x.svg",
    vector46: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-178@2x.svg",
    vector47: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-179@2x.svg",
    vector48: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-180@2x.svg",
    vector49: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-181@2x.svg",
    vector50: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-182@2x.svg",
    vector51: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-183@2x.svg",
    vector52: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-184@2x.svg",
    vector53: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-185@2x.svg",
    vector54: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-186@2x.svg",
    vector55: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-187@2x.svg",
    vector56: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-188@2x.svg",
    vector57: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-189@2x.svg",
    vector58: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-190@2x.svg",
    vector59: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-191@2x.svg",
    vector60: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-192@2x.svg",
    vector61: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-193@2x.svg",
    vector62: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-194@2x.svg",
    elmapMarkerProps: elmapMarkerData,
    elmapMarker2Props: elmapMarker2Data,
};

