import React from "react";
import "./ElmapMarker.css";

function ElmapMarker(props) {
  const { src, className } = props;

  return (
    <div className={`elmap-marker-1 ${className || ""}`}>
      <img className="vector-61" src={src} />
    </div>
  );
}

export default ElmapMarker;
