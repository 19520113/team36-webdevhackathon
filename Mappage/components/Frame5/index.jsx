import React from "react";
import ElmapMarker from "../ElmapMarker";
import "./Frame5.css";

function Frame5(props) {
  const {
    rectangle4,
    rectangle18,
    title,
    thngTinDchT,
    giPhnHi,
    bnDchT,
    tinTc,
    rectangle1,
    rectangle2,
    logIn,
    signUp,
    text1,
    polygon4,
    tnhTp,
    text2,
    vector2,
    polygon8,
    phngX,
    polygon7,
    qunHuyn,
    vector3,
    vector4,
    vector5,
    vector6,
    vector7,
    vector8,
    vector9,
    vector10,
    vector11,
    vector12,
    vector13,
    vector14,
    vector15,
    vector16,
    vector17,
    vector18,
    vector19,
    vector20,
    vector21,
    vector22,
    vector23,
    vector24,
    vector25,
    vector26,
    vector27,
    vector28,
    vector29,
    vector30,
    vector31,
    vector32,
    vector33,
    vector34,
    vector35,
    vector36,
    vector37,
    vector38,
    vector39,
    vector40,
    vector41,
    vector42,
    vector43,
    vector44,
    vector45,
    vector46,
    vector47,
    vector48,
    vector49,
    vector50,
    vector51,
    vector52,
    vector53,
    vector54,
    vector55,
    vector56,
    vector57,
    vector58,
    vector59,
    vector60,
    vector61,
    vector62,
    elmapMarkerProps,
    elmapMarker2Props,
  } = props;

  return (
    <div class="container-center-horizontal">
      <div className="frame-5 screen">
        <div className="overlap-group1">
          <img className="rectangle-4" src={rectangle4} />
          <img className="rectangle-18" src={rectangle18} />
          <h1 className="title roboto-bold-blue-whale-40px">{title}</h1>
          <div className="thng-tin-dch-t roboto-bold-blue-whale-36px">{thngTinDchT}</div>
          <div className="gi-phn-hi roboto-bold-blue-whale-36px">{giPhnHi}</div>
          <div className="bn-dch-t roboto-bold-blue-whale-36px">{bnDchT}</div>
          <div className="tin-tc roboto-bold-blue-whale-36px">{tinTc}</div>
          <img
            className="vector-29"
            src="https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector-7@2x.svg"
          />
          <img className="rectangle-1" src={rectangle1} />
          <img className="rectangle-2" src={rectangle2} />
          <div className="log-in roboto-bold-onahau-36px">{logIn}</div>
          <div className="sign-up roboto-bold-onahau-36px">{signUp}</div>
        </div>
        <div className="overlap-group5">
          <div className="image-2"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15670.21939535197!2d106.852571336006!3d10.921396194684107!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174def9d0b634dd%3A0xfa40e7d0d12f73d9!2sAn%20B%C3%ACnh%2C%20Bien%20Hoa%2C%20Dong%20Nai%2C%20Vietnam!5e0!3m2!1sen!2s!4v1618999760177!5m2!1sen!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe></div>
          <div className="overlap-group3">
            <div className="text-1 roboto-bold-blue-whale-36px">{text1}</div>
            <ElmapMarker src={elmapMarkerProps.src} />
            <div className="rectangle-14 border-3px-blue-whale"></div>
            <img className="polygon-4" src={polygon4} />
            <div className="tnhtp roboto-bold-black-36px">{tnhTp}</div>
            <div className="text-2 roboto-bold-black-36px">{text2}</div>
            <img className="vector-2" src={vector2} />
          </div>
          <ElmapMarker src={elmapMarker2Props.src} className="elmap-marker" />
          <div className="overlap-group2">
            <div className="rectangle-20 border-3px-blue-whale"></div>
            <img className="polygon-8" src={polygon8} />
            <div className="phngx roboto-bold-black-36px">{phngX}</div>
          </div>
          <div className="overlap-group">
            <div className="rectangle-19 border-3px-blue-whale"></div>
            <img className="polygon-7" src={polygon7} />
            <div className="qun-huyn roboto-bold-black-36px">{qunHuyn}</div>
          </div>
          <div className="asset-13-2">
            <div className="overlap-group4">
              <img className="vector-56" src={vector3} />
              <img className="vector-41" src={vector4} />
              <img className="vector-28" src={vector5} />
              <img className="vector-15" src={vector6} />
              <img className="vector-20" src={vector7} />
              <img className="vector-23" src={vector8} />
              <img className="vector-31" src={vector9} />
              <img className="vector-1" src={vector10} />
              <img className="vector-1" src={vector11} />
              <img className="vector-3" src={vector12} />
              <img className="vector-8" src={vector13} />
              <img className="vector-13" src={vector14} />
              <img className="vector-53" src={vector15} />
              <img className="vector-49" src={vector16} />
              <img className="vector-39" src={vector17} />
              <img className="vector-26" src={vector18} />
              <img className="vector-33" src={vector19} />
              <img className="vector-27" src={vector20} />
              <img className="vector-30" src={vector21} />
              <img className="vector-4" src={vector22} />
              <img className="vector-47" src={vector23} />
              <img className="vector-55" src={vector24} />
              <img className="vector-7" src={vector25} />
              <img className="vector-10" src={vector26} />
              <img className="vector-54" src={vector27} />
              <img className="vector-35" src={vector28} />
              <img className="vector-37" src={vector29} />
              <img className="vector-38" src={vector30} />
              <img className="vector-44" src={vector31} />
              <img className="vector-25" src={vector32} />
              <img className="vector-11" src={vector33} />
              <img className="vector-50" src={vector34} />
              <img className="vector-43" src={vector35} />
              <img className="vector-60" src={vector36} />
              <img className="vector-9" src={vector37} />
              <img className="vector-59" src={vector38} />
              <img className="vector-32" src={vector39} />
              <img className="vector-34" src={vector40} />
              <img className="vector-52" src={vector41} />
              <img className="vector-6" src={vector42} />
              <img className="vector-5" src={vector43} />
              <img className="vector-12" src={vector44} />
              <img className="vector-57" src={vector45} />
              <img className="vector-14" src={vector46} />
              <img className="vector-46" src={vector47} />
              <img className="vector-48" src={vector48} />
              <img className="vector-58" src={vector49} />
              <img className="vector-16" src={vector50} />
              <img className="vector-24" src={vector51} />
              <img className="vector-45" src={vector52} />
              <img className="vector" src={vector53} />
              <img className="vector-18" src={vector54} />
              <img className="vector-40" src={vector55} />
              <img className="vector-51" src={vector56} />
              <img className="vector-17" src={vector57} />
              <img className="vector-22" src={vector58} />
              <img className="vector-42" src={vector59} />
              <img className="vector-21" src={vector60} />
              <img className="vector-19" src={vector61} />
              <img className="vector-36" src={vector62} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Frame5;
