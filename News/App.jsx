import "./App.css";
import React from "react";
import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import Frame8 from "./components/Frame8";

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/:path(|frame-8)">
          <Frame8 {...frame8Data} />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
const frame8Data = {
    frame8: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-4@1x.svg",
    anticovi: "AntiCoVi",
    thngTinDchT: "Thông tin dịch tễ",
    giPhnHi: "Gửi phản hồi",
    bnDchT: "Bản đồ dịch tễ",
    tinTc: "Tin tức",
    overlapGroup: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-11@2x.svg",
    signUp: "Sign up",
    overlapGroup2: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-12@2x.svg",
    logIn: "Log in",
    rectangle15: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-15@1x.png",
    text1: "Bộ Y tế điều chỉnh phân bổ 110.000 liều vắc xin COVID-19 của COVAX đợt 2",
    rectangle16: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-16@2x.png",
    text2: "Thủ tướng Phạm Minh Chính: Tổ chức tiêm vắc xin COVID-19 khẩn trương, an toàn, dứt khoát không được để huỷ bỏ",
    rectangle17: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-17@2x.png",
    text3: "Bộ Y tế: Các địa phương cần sẵn sàng cho tình huống có dịch COVID-19",
    text4: "CẬP NHẬT THÔNG TIN TÌNH HÌNH DỊCH BỆNH TẠI WEBSITE CỦA BỘ Y TẾ",
    rectangle22: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/rectangle-22@2x.png",
};

