import React from "react";
import "./Frame8.css";

function Frame8(props) {
  const {
    frame8,
    anticovi,
    thngTinDchT,
    giPhnHi,
    bnDchT,
    tinTc,
    overlapGroup,
    signUp,
    overlapGroup2,
    logIn,
    rectangle15,
    text1,
    rectangle16,
    text2,
    rectangle17,
    text3,
    text4,
    rectangle22,
  } = props;

  return (
    <div class="container-center-horizontal">
      <div className="frame-8 screen" style={{ backgroundImage: `url(${frame8})` }}>
        <div className="flex-row">
          <div className="overlap-group3">
            <div className="anti-co-vi roboto-bold-blue-whale-30px">{anticovi}</div>
            <a href="#rectangle-4">
              <div className="thng-tin-dch-t roboto-bold-blue-whale-30px">{thngTinDchT}</div>
            </a>
            <div className="gi-phn-hi roboto-bold-blue-whale-30px">{giPhnHi}</div>
            <div className="bn-dch-t roboto-bold-blue-whale-30px">{bnDchT}</div>
            <div className="tin-tc roboto-bold-blue-whale-30px">{tinTc}</div>
            <img
              className="vector"
              src="https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60838b8aac9be04255064050/img/vector@2x.svg"
            />
          </div>
          <div className="overlap-group" style={{ backgroundImage: `url(${overlapGroup})` }}>
            <div className="sign-up roboto-bold-onahau-30px">{signUp}</div>
          </div>
          <div className="overlap-group2" style={{ backgroundImage: `url(${overlapGroup2})` }}>
            <div className="log-in roboto-bold-onahau-30px">{logIn}</div>
          </div>
        </div>
        <div className="overlap-group1">
          <div className="overlap-group4">
            <div className="rectangle-18 border-1px-black"></div>
            <a href="https://ncov.moh.gov.vn/vi/web/guest/-/6847912-126" target="_blank">
              <img className="rectangle-15" src={rectangle15} />
            </a>
            <div className="text-1 robotoslab-bold-black-32px">{text1}</div>
          </div>
          <div className="overlap-group5">
            <div className="rectangle-23 border-1px-black"></div>
            <a href="https://ncov.moh.gov.vn/vi/web/guest/-/6847912-127" target="_blank">
              <img className="rectangle-16" src={rectangle16} />
            </a>
            <div className="text-2 robotoslab-bold-black-32px">{text2}</div>
          </div>
          <div className="overlap-group6">
            <div className="rectangle-24 border-1px-black"></div>
            <a href="https://ncov.moh.gov.vn/vi/web/guest/-/6847912-126" target="_blank">
              <img className="rectangle-17" src={rectangle17} />
            </a>
            <div className="text-3 robotoslab-bold-black-32px">{text3}</div>
          </div>
        </div>
        <h1 className="text-4">{text4}</h1>
        <a href="https://ncov.moh.gov.vn/trang-chu" target="_blank">
          <img className="rectangle-22" src={rectangle22} />
        </a>
      </div>
    </div>
  );
}

export default Frame8;
