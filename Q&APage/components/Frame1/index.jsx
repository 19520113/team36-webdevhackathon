import React from "react";
import Frame5 from "../Frame5";
import "./Frame1.css";

function Frame1(props) {
  const {
    rectangle4,
    poster,
    rectangle6,
    vitNam,
    address,
    number,
    address2,
    number2,
    thGii,
    phone,
    phone2,
    phone3,
    phone4,
    text1,
    phatHienNguocDoiVeTacDungPhuCuaVacX,
    anticovi,
    thngTinDchT,
    giPhnHi,
    bnDchT,
    tinTc,
    rectangle11,
    rectangle12,
    logIn,
    signUp,
    tinTc2,
    vector2,
    khaiBoYT,
    frame5Props,
    frame52Props,
  } = props;

  return (
    <div class="container-center-horizontal">
      <div className="frame-1 screen">
        <div className="overlap-group">
          <div className="rectangle-8"></div>
          <img className="rectangle-4" src={rectangle4} />
          <img className="poster" src={poster} />
          <img className="rectangle-6" src={rectangle6} />
          <div className="vit-nam valign-text-middle roboto-bold-white-30px">{vitNam}</div>
          <Frame5
            sCaNhim={frame5Props.sCaNhim}
            angIuTr={frame5Props.angIuTr}
            khi={frame5Props.khi}
            tVong={frame5Props.tVong}
          />
          <Frame5
            sCaNhim={frame52Props.sCaNhim}
            angIuTr={frame52Props.angIuTr}
            khi={frame52Props.khi}
            tVong={frame52Props.tVong}
            className="frame-6"
          />
          <div className="address valign-text-middle roboto-bold-blue-whale-30px">{address}</div>
          <div className="number valign-text-middle roboto-bold-green-blue-30px">{number}</div>
          <div className="address-1 valign-text-middle roboto-bold-navy-blue-30px">{address2}</div>
          <div className="number-1 valign-text-middle roboto-bold-dodger-blue-30px">{number2}</div>
          <div className="rectangle-7"></div>
          <div className="th-gii valign-text-middle roboto-bold-white-30px">{thGii}</div>
          <div className="phone valign-text-middle roboto-bold-blue-whale-30px">{phone}</div>
          <div className="phone-3 valign-text-middle roboto-bold-green-blue-30px">{phone2}</div>
          <div className="phone-1 valign-text-middle roboto-bold-navy-blue-30px">{phone3}</div>
          <div className="phone-2 valign-text-middle roboto-bold-dodger-blue-30px">{phone4}</div>
          <div className="text-1 roboto-bold-green-blue-30px">{text1}</div>
          <img className="phat-hien-nguoc-xin-covid-19-1" src={phatHienNguocDoiVeTacDungPhuCuaVacX} />
          <div className="anti-co-vi roboto-bold-blue-whale-30px">{anticovi}</div>
          <div className="thng-tin-dch-t roboto-bold-blue-whale-30px">{thngTinDchT}</div>
          <div className="gi-phn-hi roboto-bold-blue-whale-30px">{giPhnHi}</div>
          <div className="bn-dch-t roboto-bold-blue-whale-30px">{bnDchT}</div>
          <div className="tin-tc roboto-bold-blue-whale-30px">{tinTc}</div>
          <img
            className="vector"
            src="https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60837c00d0457363ac60a8f5/img/vector@2x.svg"
          />
          <img className="rectangle-11" src={rectangle11} />
          <img className="rectangle-12" src={rectangle12} />
          <div className="log-in roboto-bold-onahau-30px">{logIn}</div>
          <div className="sign-up roboto-bold-onahau-30px">{signUp}</div>
          <div className="rectangle-9"></div>
          <h1 className="tin-tc-1">{tinTc2}</h1>
          <img className="vector-2" src={vector2} />
          <div className="khai-bo-y-t valign-text-middle">{khaiBoYT}</div>
        </div>
      </div>
    </div>
  );
}

export default Frame1;
