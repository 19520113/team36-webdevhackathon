import React from "react";
import "./Frame5.css";

function Frame5(props) {
  const { sCaNhim, angIuTr, khi, tVong, className } = props;

  return (
    <div className={`frame-5 ${className || ""}`}>
      <div className="overlap-group1">
        <div className="s-ca-nhim valign-text-middle roboto-bold-blue-whale-30px">{sCaNhim}</div>
        <div className="ang-iu-tr valign-text-middle roboto-bold-green-blue-30px">{angIuTr}</div>
        <div className="khi valign-text-middle roboto-bold-navy-blue-30px">{khi}</div>
        <div className="t-vong valign-text-middle roboto-bold-dodger-blue-30px">{tVong}</div>
      </div>
    </div>
  );
}

export default Frame5;
